from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path
from . import views
from posts.api.serializers import GroupList , UserDetails, UserList
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from posts import views as posts_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^posts/', include('posts.urls')),
    url(r'^about/', views.about),
    url(r'^$', posts_views.post_list, name='home'),
    url(r'^api/', include('posts.api.urls')),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('users/', UserList.as_view()),
    path('users/<pk>/', UserDetails.as_view()),
    path('groups/', GroupList.as_view()),
]

urlpatterns +=  staticfiles_urlpatterns()
