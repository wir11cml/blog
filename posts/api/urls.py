from django.urls import path, include
from django.conf.urls import url

from django.contrib import admin
from posts.api.serializers import GroupList , UserDetails, UserList
from .views import PostView
from rest_framework import routers

router = routers.DefaultRouter()
router.register('posts', PostView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
