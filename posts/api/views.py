from rest_framework import viewsets, permissions
from posts.models import Post
from .serializers import PostSerializer
from django.contrib.auth.models import User
from django.http import HttpResponseForbidden



class PostView(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, req):
        return super().list(req)

    def create(self, request):
        request.data["author"] = User.objects.get(username = request.user).pk
        return super().create(request)

    def update(self, request, pk=None):
        aut = Post.objects.get(pk=pk).author
        if aut == request.user:
            request.data["author"] = User.objects.get(username=request.user).pk
            return super().update(request, pk)
        else:
            return HttpResponseForbidden()

    def partial_update(self, request, pk=None):
        aut = Post.objects.get(pk=pk).author
        if aut == request.user:
            request.data["author"] = User.objects.get(username=request.user).pk
            return super().partial_update(request, pk)
        else:
            return HttpResponseForbidden()

    def destroy(self, request, pk=None):
        aut = Post.objects.get(pk=pk).author
        if aut == request.user:
            request.data["author"] = User.objects.get(username=request.user).pk
            return super().destroy(request, pk)
        else:
            return HttpResponseForbidden()
