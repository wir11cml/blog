from django.shortcuts import render, redirect
from .models import Post, Comment
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from . import forms
from oauth2_provider.decorators import protected_resource

def post_list(request):

    posts = Post.objects.all().order_by('date')

    return render(request, 'posts/post_list.html', {'posts': posts})


def article_detail(request, slug):

    post = Post.objects.get(slug=slug)
    comment = Comment.objects.filter(post=post).order_by('-id')

    if request.method == 'POST':
        comment_form = forms.CommentForm(request.POST or None)
        if comment_form.is_valid():
            content = request.POST.get('body')
            comment = Comment.objects.create(post=post, author=request.user, body=content)
            comment.save()
            return redirect('posts:list')
    else:
        comment_form = forms.CommentForm()

    return render(request, 'posts/post_detail.html', {'post': post, 'comment': comment, 'comment_form': comment_form})


@login_required(login_url="/accounts/login/")
#@protected_resource()
def create_post(request):

    if request.method == 'POST':
        form = forms.PostForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.author = request.user
            instance.save()
            return redirect('posts:list')
    else:
        form = forms.PostForm()
    return render(request, 'posts/create_post.html', {'form': form})

#def delete_post(request, slug):
    #post = Post.objects.get(slug=slug)


        #form = forms.PostForm(request.POST, instance=post)
    #post.delete()
    #return redirect('posts:list')
    #posts = Post.objects.all().order_by('date')
    #return render(request, 'posts/post_list.html', {'posts': posts})




# def create_comment(request):

    #if request.method == 'POST':
    #    form = forms.CommentForm(request.POST)
    #    if form.is_valid():
    #        instance = form.save(commit=False)
    #        post = Post.objects.get(slug=slug)
    #        instance.post = post.pk
    #        instance.save()
    #        return redirect('posts:detail')
    #else:
    #form = forms.CommentForm()
    #return render(request, 'posts/create_comment.html', {'form': form})


